#! /bin/bash

for lr in 0.01
do
    for layers in 2 3 4
    do
        for neurons in 8 32 64 128
        do
            for batch_size in 8 32 128
            do
                for activation in 'ReLU' 'Tanh'
                do
                    for add_time in 1.57 3.14 6.28 12.56
                    do
                        for delay_part in 0.0 0.25 0.5 0.75
                        do
                            
                            export name="$layers"_"$neurons"_"$batch_size"_"$lr"_"$activation"_"$add_time"_"$delay_part"_ds 
                            date
                            echo $name
                            sbatch train.sbatch model.input_shape=9 model.add_ds=True model.layers=$layers model.neurons=$neurons model.activation=\"$activation\" dataset.add_time=$add_time dataset.delay_part=$delay_part learning.name=\"$name\" learning.learning_rate=$lr train.batch_size=$batch_size
                            echo ----------------------------------------------------------------------------------------------------------------------------------
                        done
                    done
                done
                sleep 120s
            done
        done
    done
done