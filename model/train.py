import os
import numpy as np
import torch
from torch import nn
import matplotlib.pyplot as plt
import json
from pathlib import Path
import prepare_data
import torch_models
import hydra
from hydra.utils import instantiate
from omegaconf import OmegaConf, DictConfig
import logging
log = logging.getLogger('model__train')
torch.cuda.manual_seed(123)
torch.cuda.manual_seed_all(123)
np.random.seed(2134)

device = 'cuda' if torch.cuda.is_available() else 'cpu'
log.info(f'Running on {device}\n')

@hydra.main(version_base=None, config_path='config', config_name='config.yaml')
def main(cfg: DictConfig):
    yaml = OmegaConf.to_yaml(cfg)
    print(yaml)
    
    p = cfg.learning
    model_path = Path(p.path) / str(p.name)
    model_path.mkdir(parents=True, exist_ok=True)
    with open(model_path / 'config.yaml', 'w') as file:
        file.write(yaml)
    
    model = instantiate(cfg.model).to(device)
    metrics = []

    folds = ['train'] * cfg.train.folds + ['val'] * cfg.val.folds
    n_folds = len(folds) if 'n_folds' not in p.keys() else p.n_folds

    batches = []
    log.info(f'Loading data')
    trajectories, target = prepare_data.get_trajectories_and_target(**cfg['dataset'])
    idx = np.random.permutation(len(trajectories))

    batches = []
    for fold, mode in enumerate(folds):
        batches_idx = prepare_data.get_batches(idx[fold::n_folds], cfg[mode].batch_size)
        log.info(f'Fold {fold + 1}/{len(folds)} - {mode} - preparing_bathces ({len(batches_idx)} x {cfg[mode].batch_size})')
        batches.append([
            (
                [trajectories[i] for i in b],
                [target[i] for i in b],
            )
            for b in batches_idx
        ])
        
    # _ = model.fit(batches[-1], evaluation=True, log_prefix='pre_eval')
    for epoch in range(p.n_epochs):
        
        ep = f'Epoch {epoch + 1} -'
        learning_rate = p.learning_rate * (p.learning_rate_decay ** epoch)
        metrics.append({'learning_rate': learning_rate, 'train': [], 'val': []})
        log.info(f'{ep} Learning rate: {learning_rate}')

        for fold, mode in enumerate(folds):
            ep_f = f'{ep} Fold {fold + 1}/{len(folds)} - {mode} -'
            log.info(f'{ep_f} {len(batches[fold])} batches x {cfg[mode].batch_size}')
            if mode == 'train':
                m = model.fit(batches[fold], lr=learning_rate, log_prefix=ep_f)
                torch.save(model.state_dict(), model_path / f'{epoch + 1}_{fold + 1}.pt')
            elif mode == 'val':
                m = model.fit(batches[fold], evaluation=True, log_prefix=ep_f)
            
            metrics[epoch][mode].append(m)
            with open(model_path / 'metrics.json', 'w') as file:
                json.dump(metrics, file)

if __name__ == '__main__':
    main()