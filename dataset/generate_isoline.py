import gc
import logging
import pathlib
import pickle
import time

import hydra
import matplotlib.pyplot as plt
import numpy as np
import orbipy as op
import pandas as pd
import torch
from custom_corrections import (border_correction, jacobi_correction,
                                zero_correction)
from hydra.utils import instantiate
from omegaconf import DictConfig, OmegaConf
from torch_models import custom_event

log = logging.getLogger(__name__)

device = 'cuda' if torch.cuda.is_available() else 'cpu'
log.info(f'Running on {device}\n')


def station_keeper_prop(station_keeper, s, n_corrections, figure_path):
    if figure_path is not None:
        plt.scatter(s[0], s[2], c='k', s=10)
        plt.savefig(figure_path)
    try:
        _ = station_keeper.prop(0.0, s, N=n_corrections)
    except:
        pass

    dvout = np.array(station_keeper.dvout)
    dv = np.mean(np.linalg.norm(dvout[:, -3:], axis=1))
    N = int(dvout.shape[0])
    log.info(f'N = {N} dv = {dv}')
    if figure_path is not None:
        if N == n_corrections:
            if dv < 1e-11:
                c = 'g'
            else:
                c = 'b'
        else:
            c = 'r'
        plt.scatter(s[0], s[2], c=c, s=15)
        plt.savefig(figure_path)
    return [*s, dv, N], station_keeper.arr


def get_model_custom_event(weights_path: pathlib.PosixPath, threshold: float):
    cfg = OmegaConf.load(weights_path.parent / 'config.yaml').model
    cfg.weights_path = weights_path
    model = instantiate(cfg).to(device)
    return custom_event(model=model, value=threshold)


def iteration(iteration: int, cfg: DictConfig):
    path = pathlib.Path(f'{cfg.data_path}/{iteration}/{cfg.side}')
    path.mkdir(parents=True, exist_ok=True)
    figure_path = path / 'plot.png' if cfg.plot else None
    plt.clf()

    op_model = op.crtbp3_model()
    direction = op.unstable_direction_stm(op.crtbp3_model(stm=True), ignore_z=True)
    if cfg.left == 'border':
        left = op.eventX(op_model.L1 - 1500000 / op_model.R)
    else:
        left = get_model_custom_event(pathlib.Path(cfg.left), -np.tanh(cfg.left_threshold))
    if cfg.right == 'border':
        right = op.eventX(1 - 10000/op_model.R)
    else:
        right = get_model_custom_event(pathlib.Path(cfg.right), np.tanh(cfg.right_threshold))
    
    if 'zero_velocity_line_file' in cfg:
        zero_velocity_line_file = pathlib.Path(cfg.zero_velocity_line_file)
    else:
        zero_velocity_line_file = pathlib.Path(cfg.data_path) / 'zero_velocity_line.csv'
    log.info(f'iteration {iteration} - zero_velocity_line waiting')
    wait_iters = 0
    while wait_iters < 30:
        try:
            if zero_velocity_line_file.exists():
                zvl = pd.read_csv(zero_velocity_line_file).values
                if len(zvl) > iteration:
                    s0 = zvl[iteration, :6]
                    break
                log.info(f'iteration {iteration} - zero_velocity_line waiting ({len(zvl)} is ready)')
        except:
            pass
        time.sleep(60)
        wait_iters += 1
    
    
    r_to_L1 = np.linalg.norm([s0[0] - op_model.L1, *s0[1:]])
    radius = (cfg.radius_coef * r_to_L1 ** cfg.radius_degree)
    log.info(f'iteration {iteration} - r_to_L1: {round(r_to_L1 * op_model.R, 2)}')
    log.info(f'iteration {iteration} - radius:  {round(radius * op_model.R, 2)}')
    
    bc = border_correction(
        direction=direction, left=left, right=right, 
        reintegrate=cfg.reintegrate, 
        save_data=cfg.save_data,
        save_trajectories=cfg.save_trajectories,
        add_time=cfg.add_time_pi * np.pi,
        use_detector=cfg.use_detector
    )
    jc = jacobi_correction(
        v_sign=-1, r=radius, 
        parameter_initial=0, parameter_delta=cfg.jacobi_angle_delta_pi * np.pi, 
        left=left, right=right,
        reintegrate=cfg.reintegrate, 
        save_data=cfg.save_data,
        save_trajectories=cfg.save_trajectories,
        add_time=cfg.add_time_pi * np.pi,
        use_detector=cfg.use_detector
    )

    station_keeper = op.simple_station_keeping(
        op_model, zero_correction(), bc, rev=cfg.rev_pi * np.pi, 
        events=[op.eventY(terminal=False), op.eventZ(terminal=False)], verbose=False
    )
    points = [s0]

    if cfg.side == 'left':
        jc.parameter_initial = np.pi
        jc.v_sign = 1
    elif cfg.side == 'right':
        jc.parameter_initial = 0
        jc.v_sign = -1
    log.info(f'iteration {iteration} - j = 0 - started')
    df_C_element, sk_arr_element = station_keeper_prop(station_keeper, s0, cfg.n_corrections, figure_path)
    df_C = [df_C_element]
    sk_arr = [sk_arr_element]
    output = dict((key, jc.data[key] + bc.data[key]) for key in jc.data.keys())
    df = []
    for corr in (jc, bc):
        corr_name = corr.__class__.__name__
        df += [
            (
                corr_name, 0, j, *corr.data['initial_condition'][j],
                corr.data['time'][j], corr.data['sign'][j]
            ) for j in range(corr.data_size)
        ]

    jc.delete_data()
    bc.delete_data()
    gc.collect()

    exception_flg = False

    for i in range(1, cfg.max_iterations):
        log.info(f'iteration {iteration} - j = {i} - started')
        s0 = points[-1].copy()
        
        try:
            s0 = s0 + jc.calc_dv(0, s0)
            points.append(s0)
            df_C_element, sk_arr_element = station_keeper_prop(station_keeper, s0, cfg.n_corrections, figure_path)
            df_C.append(df_C_element)
            sk_arr.append(sk_arr_element)
            for key in output.keys():
                output[key] += jc.data[key] + bc.data[key]
            for corr in (jc, bc):
                corr_name = corr.__class__.__name__
                df += [
                    (
                        corr_name, i, j, *corr.data['initial_condition'][j],
                        corr.data['time'][j], corr.data['sign'][j]
                    ) for j in range(corr.data_size)
                ]
            jc.delete_data()
            bc.delete_data()

        except:
            exception_flg = True

        if (s0[2] < 0) or exception_flg:
            break
    df_C = pd.DataFrame(df_C, columns=['x', 'y', 'z', 'vx', 'vy', 'vz', 'dv', 'N'])
    df_C.index.name = 'df_C_iteration'
    df_C.to_csv(path/'df_C.csv')
    if cfg.save_orbits:
        with open(path / 'orbits.pickle', 'wb') as file:
            pickle.dump(sk_arr, file)

    if cfg.save_data:
        log.info(f'iteration {iteration} - Saving df.csv')
        df = pd.DataFrame(
            df, columns=[
                'correction_type', 'df_C_iteration', 'correction_idx',
                't', 'x', 'y', 'z', 'vx', 'vy', 'vz', 'time', 'sign'
            ]
        )
        df.index.name = 'j'
        df.to_csv(path/'df.csv')
    if cfg.save_trajectories:  
        for key in output:
            log.info(f'iteration {iteration} - Saving {key}.npy')
            if 'trajectory' in key:
                output[key] = np.asanyarray(output[key], dtype='object')
            np.save(path / key, output[key], allow_pickle=True)
    log.info(f'iteration {iteration} - Successfully finished!\n\n')


@hydra.main(version_base=None, config_path='config', config_name='data.yaml')
def main(cfg: DictConfig):
    print(OmegaConf.to_yaml(cfg))
    for i in range(cfg.iteration_start + cfg.iteration_fold, cfg.iteration_finish, cfg.iteration_step):
        iteration(i, cfg)


if __name__ == '__main__':
    main()
