import gc
import logging
import pathlib

import hydra
import matplotlib.pyplot as plt
import numpy as np
import orbipy as op
import pandas as pd
import torch
from custom_corrections import (border_correction, jacobi_correction,
                                zero_correction, zero_speed_correction)
from hydra.utils import instantiate
from omegaconf import DictConfig, OmegaConf
from torch_models import custom_event
from tqdm import trange

log = logging.getLogger(__name__)

device = 'cuda' if torch.cuda.is_available() else 'cpu'
log.info(f'Running on {device}\n')


def get_model_custom_event(weights_path: pathlib.PosixPath, threshold: float):
    cfg = OmegaConf.load(weights_path.parent / 'config.yaml').model
    cfg.weights_path = weights_path
    model = instantiate(cfg).to(device)
    return custom_event(model=model, value=threshold)

def iteration(cfg: DictConfig):
    path = pathlib.Path(f'{cfg.data_path}/')
    path.mkdir(parents=True, exist_ok=True)

    op_model = op.crtbp3_model()
    if cfg.left == 'border':
        left = op.eventX(op_model.L1 - 1500000 / op_model.R)
    else:
        left = get_model_custom_event(pathlib.Path(cfg.left), -np.tanh(cfg.left_threshold))
    if cfg.right == 'border':
        right = op.eventX(1 - 10000/op_model.R)
    else:
        right = get_model_custom_event(pathlib.Path(cfg.right), np.tanh(cfg.right_threshold))
    
    
    old_points = pd.read_csv('zero_velocity_line.csv').iloc[:1520]
    # old_points = pd.read_csv(pathlib.Path(f'{cfg.old_data_path}/')/ 'zero_velocity_line.csv').iloc[:1870]
    s0 = old_points.values[-1, :6]
    dx, dz = (old_points.iloc[-1] - old_points.iloc[-2])[['x', 'z']].values
    alpha0 = np.arctan(dz/dx)


    zsc = zero_speed_correction(r=1500/op_model.R, parameter_initial=alpha0, left=left, right=right)

    points = [s0]
    log.info(f'zero_velocity_line')
    for i in range(600):
        try:
            points.append(points[-1] + zsc.calc_dv(0, points[-1]))
            points_1 = pd.DataFrame(points, columns=['x', 'y', 'z', 'vx', 'vy', 'vz'])
            points_1['C'] = op_model.jacobi(points_1.values)
            points_1 = pd.concat([old_points, points_1.iloc[1:]])
            points_1.to_csv(path / 'zero_velocity_line.csv', index=False)
            log.info(f'iteration {len(points_1)} finished')
        except:
            break




@hydra.main(version_base=None, config_path='config', config_name='data.yaml')
def main(cfg: DictConfig):
    print(OmegaConf.to_yaml(cfg))
    iteration(cfg)


if __name__ == '__main__':
    main()