import os
import pathlib

import numpy as np
import pandas as pd
from tqdm import tqdm, trange

path = pathlib.Path('corrections/models_grid_search')

models = os.listdir(path)

data = {}
for model in tqdm(models):
    folder = path / model
    data[model] = {}
    iters = sorted([int(i) for i in os.listdir(folder) if 'csv' not in i])
    for ii in iters:
        iteration = folder / str(ii)
        data[model][ii] = {}
        for side in ('left', 'right'):
            p = iteration / side / 'df_C.csv'
            if p.exists():
                data[model][ii][side] = pd.read_csv(p, index_col=0)
        if not data[model][ii]:
            data[model].pop(ii)
        else:
            data[model][ii] = pd.concat(data[model][ii])
    data[model] = pd.concat(data[model])

data = pd.concat(data)
data.to_csv(path / 'results.csv')