import logging
from pathlib import Path

import numpy as np
import torch

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

DATAPATH = Path('/home/saksenov/LPO/dataset/data')

def get_batches(idx, batch_size):
    batches = np.array_split(idx, len(idx) // batch_size + 1)
    batches = [b for b in batches if len(b) > 0]
    return batches

def get_trajectories(name, device=None, datapath=DATAPATH):
    path = Path(datapath) / str(name)
    with open(path / 'trajectory_before.npy', 'rb') as file:
        trajectories = list(np.load(file, allow_pickle=True))
    with open(path / 'trajectory_after.npy', 'rb') as file:
        trajectories_after = list(np.load(file, allow_pickle=True))
        
    n = len(trajectories)
    for i in range(n):
        increment = trajectories_after.pop(0)
        try:
            trajectories[i] = np.vstack((trajectories[i], increment))
        except:
            log.warning('Error while adding increment')

    if device is not None:
        for i in range(n):
            trajectories[i] = torch.tensor(trajectories[i]).to(device).float()
    
    return trajectories


def get_t_intersect(name, datapath=DATAPATH):
    with open(Path(datapath) / str(name) / 'time.npy', 'rb') as file:
        t_intersect = np.load(file, allow_pickle=True)
    return t_intersect


def get_sign(name, datapath=DATAPATH):
    with open(Path(datapath) / str(name) / 'sign.npy', 'rb') as file:
        sign = np.load(file, allow_pickle=True)
    return sign


def get_target(t, t_intersect, sign, lmbda=2, tanh=True, delay=0):

    if not t_intersect:
        target = np.zeros(len(t))
    elif lmbda is not None:
        target = sign * np.exp(lmbda * (t - t_intersect - delay))
        if tanh:
            target = np.tanh(target)
    else:
        target = np.full(len(t), sign)
    
    return target


def get_trajectories_and_target(
    name, trajectory_path=DATAPATH, time_sign_path=DATAPATH,
    lmbda=2, tanh=True, add_time=0.5*np.pi, device=None, delay_part=0
):

    trajectories = get_trajectories(name, datapath=trajectory_path)
    t_intersect = get_t_intersect(name, datapath=time_sign_path)
    sign = get_sign(name, datapath=time_sign_path)
    n = len(trajectories)

    if add_time is not None:
        for i in range(n):
            if t_intersect[i]:
                mask = (trajectories[i][:, 0] <= (t_intersect[i] + add_time))
                trajectories[i] = trajectories[i][mask]

    target = [
        get_target(
            t=trajectories[i][:, 0], t_intersect=t_intersect[i], sign=sign[i],
            lmbda=lmbda, tanh=tanh, delay=delay_part * add_time
        )
        for i in range(n)
    ]

    if device is not None:
        for i in range(n):
            trajectories[i] = torch.tensor(trajectories[i]).to(device).float()
            target[i] = torch.tensor(target[i]).to(device).float()

    return trajectories, target