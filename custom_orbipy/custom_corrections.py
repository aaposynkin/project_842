import gc

import numpy as np
import orbipy as op
from scipy.optimize import bisect


class base_correction(op.corrections.base_correction):
    def __init__(
        self, 
        parameter_initial: float, 
        parameter_delta: float,
        left: op.base_event,
        right: op.base_event,
        model: op.models.crtbp3_model = op.crtbp3_model(),
        parameter_max_iterations: int = 20,
        bisect_max_iterations: int = 100, 
        bisect_tol: float = 1e-16,
        maxt: float = 10 * np.pi,
        add_time:float = 4 * np.pi,
        reintegrate: bool = False,
        save_data: bool = False,
        save_trajectories: bool = False,
        use_detector: bool = False
    ):
        
        self.parameter_initial = parameter_initial
        self.parameter_delta = parameter_delta
        self.parameter_max_iterations = parameter_max_iterations
        
        self.bisect_max_iterations = bisect_max_iterations
        self.bisect_tol = bisect_tol

        self.model = model
        self.left, self.right = left, right
        self.det = op.event_detector(self.model, [self.left, self.right], tol=self.bisect_tol)

        self.maxt = maxt
        self.add_time = add_time
        
        self.reintegrate = reintegrate
        self.use_detector = use_detector

        if save_trajectories and not save_data:
            raise ValueError("Can't save trajectories without saving data")

        self.save_trajectories = save_trajectories
        self.save_data = save_data
        self.data_size = 0
        self.data = {
            'initial_condition': [],
            'time': [],
            'sign': []
        }
        if save_trajectories:
            self.data['trajectory_before'] = []
            self.data['trajectory_after'] = []

    def delete_data(self):
        for key in self.data:
            self.data[key] = []
        self.data_size = 0
        gc.collect()

    def get_vector(self, parameter: float, s0: np.ndarray):
        return s0

    def get_sign(self, parameter: float, t: float, s0: np.ndarray):
        vec = self.get_vector(parameter, s0)
        if vec is not None:
            if self.use_detector:
                for event in self.det.events:
                    if 'reset_state' in dir(event):
                        event.reset_state()
                trajectory_before, ev = self.det.prop(vec, t, t + self.maxt, ret_df=False)
                if ev.size > 0: # if event was detected
                    sign = -1 if ev[0][0] == 0 else 1
                    t_intersect = ev[0][3]  # event time
                else:
                    sign = 0
                    t_intersect = None
            else:
                trajectory = self.model.prop(vec, t, t + self.maxt, ret_df=False)
                model_prediction = self.det.events[0].model.predict_unstable_component(trajectory)
                ev_out = []
                for ev in self.det.events:
                    pr = model_prediction - ev.value
                    mask = (pr[1:] * pr[:-1] < 0)
                    if mask.sum() > 0:
                        first_idx = np.where(mask)[0][0]
                        ev_out.append(trajectory[first_idx, 0])
                    else:
                        ev_out.append(None)
                if ev_out[0] is None:
                    if ev_out[1] is None:
                        sign = 0
                        t_intersect = None
                    else:
                        sign = 1
                        t_intersect = ev_out[1]
                else:
                    if (ev_out[1] is None) or (ev_out[0] < ev_out[1]):
                        sign = -1
                        t_intersect = ev_out[0]
                    else:
                        sign = 1
                        t_intersect = ev_out[1]
                if t_intersect is not None:
                    trajectory_before = trajectory[:(first_idx + 1)]
                else:
                    trajectory_before = trajectory
            if self.save_data:
                self.data['initial_condition'].append([t, *vec])
                self.data['time'].append(t_intersect)
                self.data['sign'].append(sign)
                self.data_size += 1
            if self.save_trajectories:
                trajectory_after = np.asarray([[]])
                if sign != 0:
                    s_last = trajectory_before[-1]
                    add_time = self.add_time - (s_last[0] - t_intersect)
                    if add_time > 0:
                        if self.reintegrate:
                            if 'reset_state' in dir(event):
                                event.reset_state()
                            trajectory = self.model.prop(
                                vec, t, t_intersect + self.add_time, ret_df=False
                            )
                            before_mask = (trajectory[:, 0] <= t_intersect)
                            trajectory_before = trajectory[before_mask]
                            trajectory_after = trajectory[~before_mask]
                        else:
                            trajectory_after = self.model.prop(
                                s_last[1:], s_last[0], s_last[0] + add_time, ret_df=False
                            )[1:]
                self.data['trajectory_before'].append(trajectory_before)
                self.data['trajectory_after'].append(trajectory_after)
            return sign
        else:
            return None
    
    def get_grid(self, iteration: int):
        pass
    
    def calc_dv(self, t: float, s0: np.ndarray):
        for iteration in range(self.parameter_max_iterations):
            parameters = self.get_grid(iteration)
            sign = np.asarray([self.get_sign(parameter, t, s0) for parameter in parameters], dtype='float64')
            num_zeros = sum(sign == 0)
            if num_zeros > 1:
                raise RuntimeError('More than 1 zero')
            elif num_zeros == 1:
                idx_zero = np.where(sign == 0)[0][0]
                return self.get_vector(parameters[idx_zero], s0) - s0
            else:
                diff_sign_mask = (sign[:-1] * sign[1:] < 0)
                num_zeros = sum(diff_sign_mask)
                if num_zeros > 1:
                    raise RuntimeError('More than 1 zero')
                elif num_zeros == 1:
                    idx_diff = np.where(diff_sign_mask)[0][0]
                    a, b = parameters[idx_diff:idx_diff + 2]
                    self.parameter_initial = bisect(
                        self.get_sign, a, b, args=(t, s0),
                        maxiter=self.bisect_max_iterations,
                        xtol=self.bisect_tol
                    )
                    return self.get_vector(self.parameter_initial, s0) - s0
        # print(sign)
        raise RuntimeError('Maximum iterations number reached')


class zero_correction(op.corrections.base_correction):
    def __init__(self):
        super().__init__(op.models.base_model(), op.directions.base_direction())

    def calc_dv(self, t: float, s0: np.ndarray):
        return np.zeros(6)


class border_correction(base_correction):
    def __init__(
        self, 
        direction: op.directions.base_direction, 
        parameter_delta: float = 1e-12,
        *args, **kwargs
    ):
        super().__init__(
            parameter_initial=0,
            parameter_delta=parameter_delta,
            *args, **kwargs
        )
        self.direction = direction
    
    def get_vector(self, dv: float, s0: np.ndarray):
        vec = s0.copy()
        vec[3:6] += self.direction(0, s0) * dv
        return vec
    
    def get_grid(self, iteration: int):
        deg = np.power(10.0, iteration + 1)
        return deg * np.asarray([-self.parameter_delta, self.parameter_delta])


class zero_speed_correction(base_correction):
    def __init__(
        self, 
        r: float, 
        parameter_initial: float, 
        parameter_num_points: int = 6, 
        parameter_delta: float = np.pi/7,
        *args, **kwargs
    ):
        super().__init__(
            parameter_initial=parameter_initial,
            parameter_delta=parameter_delta,
            *args, **kwargs
        )
        self.r = r
        self.parameter_num_points = parameter_num_points
    
    def get_vector(self, alpha: float, s0: np.ndarray):
        vec = self.model.get_zero_state()
        vec[0] = s0[0] + self.r * np.cos(alpha)
        vec[2] = s0[2] + self.r * np.sin(alpha)
        return vec
    
    def get_grid(self, iteration: int):
        return self.parameter_initial + np.linspace(
            -self.parameter_delta, self.parameter_delta, self.parameter_num_points * (iteration + 1)
        )


class jacobi_correction(zero_speed_correction):
    def __init__(
        self, v_sign, r, parameter_initial, C=None,
        **kwargs
    ):
        super().__init__(
            r=r,
            parameter_initial=parameter_initial,
            **kwargs
        )
        self.v_sign = v_sign
        self.C = C
    
    def get_vector(self, alpha: float, s0: np.ndarray):
        vec = super().get_vector(alpha, s0)
        C = self.model.jacobi(s0) if self.C is None else self.C
        v2 = 2 * self.model.omega(vec) - C
        if v2 >= 0:
            vec[4] = self.v_sign * np.sqrt(v2)
            return vec
        else:
            return None


class zero_speed_correction_C(base_correction):
    def __init__(
        self, 
        dC: float, 
        max_r: float,
        parameter_initial: float, 
        parameter_num_points: int = 6, 
        parameter_delta: float = np.pi/7,
        *args, **kwargs
    ):
        super().__init__(
            parameter_initial=parameter_initial,
            parameter_delta=parameter_delta,
            *args, **kwargs
        )
        self.dC = dC
        self.max_r = max_r
        self.parameter_num_points = parameter_num_points
    
    def get_vector(self, alpha: float, s0: np.ndarray):

        C = self.model.jacobi(s0) + self.dC
        def func(r):
            vec = self.model.get_zero_state()
            vec[0] = s0[0] + r * np.cos(alpha)
            vec[2] = s0[2] + r * np.sin(alpha)
            return self.model.jacobi(vec) - C

        r = bisect(func, 0, self.max_r, xtol=1e-16)
        vec = self.model.get_zero_state()
        vec[0] = s0[0] + r * np.cos(alpha)
        vec[2] = s0[2] + r * np.sin(alpha)
        return vec
    
    def get_grid(self, iteration: int):
        return self.parameter_initial + np.linspace(
            -self.parameter_delta, self.parameter_delta, self.parameter_num_points * (iteration + 1)
        )


class alpha_correction_C(base_correction):
    def __init__(
        self, 
        alpha: float, 
        dC: float,
        v_sign: float,
        parameter_delta: float = 1e-12,
        *args, **kwargs
    ):
        super().__init__(
            parameter_initial=0,
            parameter_delta=parameter_delta,
            *args, **kwargs
        )
        self.alpha = alpha
        self.dC = dC
        self.v_sign = v_sign
    
    def get_vector(self, r: float, s0: np.ndarray):
        C = self.model.jacobi(s0) + self.dC
        vec = np.zeros(6)
        if self.alpha == 0:
            vec[0] = s0[0] + r
            vec[2] = s0[2]
        elif self.alpha == np.pi:
            vec[0] = s0[0] - r
            vec[2] = s0[2]
        else:
            vec[0] = s0[0] + r * np.cos(self.alpha)
            vec[2] = s0[2] + r * np.sin(self.alpha)
        v2 = 2 * self.model.omega(vec) - C
        if v2 >= 0:
            vec[4] = self.v_sign * np.sqrt(v2)
            return vec
        else:
            return None
    
    def get_grid(self, iteration: int):
        deg = np.power(10.0, iteration + 1)
        return deg * np.asarray([0, self.parameter_delta])