import gc
import logging
import time
from collections import OrderedDict

import numpy as np
import torch
from orbipy import base_event
from sklearn.metrics import accuracy_score
from torch import nn

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)



class BaseModel(nn.Module):
    def __init__(
        self, center='M', 
        add_ds=False
    ):
        super().__init__()
        
        coords = {
            'M': [0.0, 0.0, 0.0],
            'm': [1.0, 0.0, 0.0],
            'L1': [0.9900289479871328, 0.0, 0.0],
            'L2': [1.0100317349912604, 0.0, 0.0],
            'L3': [-1.000001250561829, 0.0, 0.0],
            'L4': [0.4999969986516103, 0.8660254037844386, 0.0],
            'L5': [0.4999969986516103, -0.8660254037844386, 0.0]
        }
        
        if center not in coords:
            raise ValueError(
                "center must be in ('"+ "', '".join(coords.keys()) + "')"
            )
        self.center = torch.tensor(coords[center])
        self.add_ds = add_ds

    def get_ds(self, trajectory):
        mu2 = 3.001348389698916e-06
        mu1 = 1 - mu2
        x, y, z, vx, vy, vz = [trajectory[:, i] for i in range(6)]

        yz2 = y * y + z * z
        r13 = ((x + mu2) * (x + mu2) + yz2) ** (-1.5)
        r23 = ((x - mu1) * (x - mu1) + yz2) ** (-1.5)

        mu12r12 = (mu1 * r13 + mu2 * r23)

        ax = 2 * vy + x - (mu1 * (x + mu2) * r13 + mu2 * (x - mu1) * r23)
        ay = -2 * vx + y - mu12r12 * y
        az = - mu12r12 * z
        return [ax[:, None], ay[:, None], az[:, None]]

    def get_device(self):
        return next(self.parameters()).device
    
    def get_sequential_part(
        self, input_shape, output_shape, i, 
        bn_before_linear, bn_after_linear, activation
    ):
        output = []

        if bn_before_linear:
            output.append(
                (f'{i}___bn_before_linear', nn.BatchNorm1d(input_shape))
            )
        
        output.append(
            (f'{i}___linear', nn.Linear(input_shape, output_shape)),
        )

        if bn_after_linear:
            output.append(
                (f'{i}___bn_after_linear', nn.BatchNorm1d(output_shape))
            )
        if activation is not None:
            output.append(
                (f'{i}___activation', getattr(nn, activation)()),
            )

        return output

    def load_weights(self, weights_path):
        log.info(f'Loading weights from {weights_path}')
        self.load_state_dict(
            torch.load(
                weights_path, map_location=torch.device(self.get_device())
            )
        )

    def prepare(self, trajectory, to_tensor=False):
        out = torch.clone(trajectory)
        out[:, 1:4] = out[:, 1:4] - self.center.to(out.device)
        out = out[:, 1:]
        if self.add_ds:
            out = torch.concat([out] + self.get_ds(out), axis=1)
        return out
    
    def forward(self, trajectory, prev_state, return_state):
        pass
    
    def to_numpy(self, x):
        return x.cpu().detach().numpy() 
    
    def to_tensor(self, x):
        return torch.tensor(x).to(self.get_device()).float()
  
    def predict_unstable_component(self, trajectory, prev_state=None, return_state=False, to_numpy=True):
        if torch.is_tensor(trajectory):
            trajectory_tensor = trajectory
        else:
            trajectory_tensor = self.to_tensor(trajectory)
        self.eval()
        with torch.no_grad():
            return self.forward(trajectory_tensor, prev_state, return_state, to_numpy)

    def get_class_from_unstable_component(self, unstable_component, threshold=np.tanh(1)):
        uc = unstable_component if isinstance(unstable_component, np.ndarray) else self.to_numpy(unstable_component)
        threshold_mask = (abs(uc) >= threshold)
        return np.sign(uc[threshold_mask][0]) if threshold_mask.sum() > 0 else 0

    def fit(
        self, batches, 
        criterion=nn.MSELoss(reduction='sum'), 
        optimizer=torch.optim.Adam,
        lr=1e-3, evaluation=False,
        metrics={'accuracy': accuracy_score},
        threshold=np.tanh(1),
        log_prefix=''
    ):
        output = {}
        if not evaluation:
            self.train()
            opt = optimizer(self.parameters(), lr=lr)
        else:
            self.eval()
    
        loss, n_points = 0, 0
        trajectories_true, trajectories_pred = [], []
        points_true, points_pred = [], []

        n_batches = len(batches)
        for iteration, (X_numpy, y_numpy) in enumerate(batches):
        # for iteration in range(n_batches):
            # X, y = batches.pop(0)
            
            X = [self.to_tensor(x) for x in X_numpy]
            y = [self.to_tensor(yy) for yy in y_numpy]

            if iteration % max(1, (n_batches // 50)) == 0:
                log.info(f'{log_prefix} {iteration + 1} / {n_batches}')

            y_cat = torch.cat(y)

            if not evaluation:
                opt.zero_grad()
                out = [self.forward(x) for x in X]
                out_cat = torch.cat(out)
                loss_b = criterion(out_cat, y_cat)
                loss_b.backward()
                opt.step()
            else:
                with torch.no_grad():
                    out = [self.forward(x) for x in X]
                    out_cat = torch.cat(out)
                    loss_b = criterion(out_cat, y_cat)

            loss += loss_b.item()
            n_points += len(y_cat)

            y_cat = self.to_numpy(y_cat)
            out_cat = self.to_numpy(out_cat)

            mask = abs(y_cat) >= threshold
            points_true.append(np.sign(y_cat[mask]))
            points_pred.append(np.sign(out_cat[mask]))

            trajectories_true.append(
                [self.get_class_from_unstable_component(yy) for yy in y]
            )
            trajectories_pred.append(
                [self.get_class_from_unstable_component(yy) for yy in out]
            )

            if len(points_pred) > 50:
                points_true = [np.hstack(points_true)]
                points_pred = [np.hstack(points_pred)]
                trajectories_true = [np.hstack(trajectories_true)]
                trajectories_pred = [np.hstack(trajectories_pred)]
            
            # del X, y
        gc.collect()

        loss = loss / n_points
        points_true = np.hstack(points_true)
        points_pred = np.hstack(points_pred)
        trajectories_true = np.hstack(trajectories_true)
        trajectories_pred = np.hstack(trajectories_pred)
        log.info(f'{log_prefix} Loss: {loss}')
        output['loss'] = loss

        for m in metrics:
            trajectories_metric = metrics[m](trajectories_true, trajectories_pred)
            points_metric = metrics[m](points_true, points_pred)

            log.info(f'{log_prefix} Trajectories {m} ' + '{:.9f}'.format(trajectories_metric))
            log.info(f'{log_prefix} Points {m}       ' + '{:.9f}\n'.format(points_metric))

            output[f'trajectory {m}'] = trajectories_metric
            output[f'points {m}'] = points_metric

        return output


class LSTMReg(BaseModel):
    def __init__(
        self, input_shape, 
        layers, neurons,
        bn_before_linear, bn_after_linear,
        activation, tanh, weights_path=None,
        **kwargs
    ):
        super().__init__(**kwargs)

        
        self.name = f'{layers}_{neurons}_{layers}_{layers}_{neurons}_{activation}'
        self.activation = activation
        self.tanh = tanh
        

        fc_pre = self.get_sequential_part(
            input_shape, neurons, 0,
            bn_before_linear, bn_after_linear, activation
        )

        for i in range(1, layers):
            fc_pre += self.get_sequential_part(
                neurons, neurons, i,
                bn_before_linear, bn_after_linear, activation
            )
        
        self.fc_pre = nn.Sequential(OrderedDict(fc_pre))

        self.lstm = nn.LSTM(
            input_size=neurons,
            hidden_size=neurons,
            num_layers=layers,
            batch_first=True
        )

        fc_after = []
        for i in range(layers - 1):
            fc_after += self.get_sequential_part(
                neurons, neurons, layers + i,
                bn_before_linear, bn_after_linear, activation
            )

        fc_after += self.get_sequential_part(
            neurons, 1, 2 * layers - 1,
            bn_before_linear, bn_after_linear, 
            activation='Tanh' if self.tanh else None
        )

        self.fc_after = nn.Sequential(OrderedDict(fc_after))

        if weights_path:
            self.load_weights(weights_path)

    def forward(self, trajectory, prev_state=None, return_state=False, to_numpy=False):
        out = self.prepare(trajectory)
        out = self.fc_pre(out)
        out, state = self.lstm(out) if prev_state is None else self.lstm(out, prev_state)
        out = self.fc_after(out)[:, 0]
        if to_numpy:
            out = self.to_numpy(out)
        return (out, state) if return_state else out


class FCNN(BaseModel):
    def __init__(
        self, input_shape, 
        layers, neurons,
        bn_before_linear, bn_after_linear,
        activation, tanh, weights_path=None,
        **kwargs
    ):
        super().__init__(**kwargs)
        
        self.name = f'{layers}_{neurons}_{activation}'
        self.activation = activation
        self.tanh = tanh
            
        fc = self.get_sequential_part(
            input_shape, neurons, 0,
            bn_before_linear, bn_after_linear, activation
        )

        for i in range(1, layers - 1):
            fc += self.get_sequential_part(
                neurons, neurons, i,
                bn_before_linear, bn_after_linear, activation
            )
        
        fc += self.get_sequential_part(
            neurons, 1, layers - 1,
            bn_before_linear, bn_after_linear, 
            activation='Tanh' if self.tanh else None
        )

        self.fc = nn.Sequential(OrderedDict(fc))
        if weights_path:
            self.load_weights(weights_path)
    
    def forward(self, trajectory, prev_state=None):
        out = self.prepare(trajectory)
        out = self.fc(out)[:, 0]
        return (out, None) if return_state else out


class custom_event(base_event):
    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        self.state = []

    def get_value(self, x, prev_state=None):
        val, state = self.model.predict_unstable_component(
            x, prev_state=prev_state, return_state=True
        )
        return val - self.value, state

    def __call__(self, t, s):
        x = np.asarray([[t, *s]])
        while self.state:
            prev_t, prev_state, prev_val = self.state[-1]
            if prev_t < t:
                val, state = self.get_value(x, prev_state)
                detected_flg = (val * prev_val) < 0
                self.state.append([t, state, val])
                return val
            else:
                self.state.pop()
        val, state = self.get_value(x)
        self.state.append([t, state, val])
        return val
    
    def reset_state(self):
        self.state = [](ml_env)